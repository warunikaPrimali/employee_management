import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Constants } from 'src/app/app-constants';
import { ValidationService } from 'src/app/core/validation.service';
import { ToasterService } from 'src/app/shared/toaster.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public user;
  public loginForm: FormGroup;
  public appVarsion;

  constructor(private fb: FormBuilder, private router: Router, private toasterService: ToasterService, private validationService: ValidationService) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      name: ['', [Validators.required, this.validationService.nameValidator, this.validationService.whiteSpaceChecker]],
      password: ['', [Validators.required]]
    });
    this.appVarsion = Constants.appVersions.android;
  }

  /**
  * Method to call on login
  * Check user entered credential with Hard coded values  
  * set status to sessionStorage
  * show a toster if login Failed
  */
  login() {
    const user = Constants.userCredentials;
    if (user.name === this.loginForm.get('name').value && user.password === this.loginForm.get('password').value) {
      sessionStorage.setItem(Constants.logged, 'true');
      sessionStorage.setItem(Constants.loggedTime, JSON.stringify(new Date().toLocaleString()))
      this.router.navigate([Constants.routeDashboard]);
    } else {
      const data = {
        message: 'Invalid username or password',
        color: 'danger'
      }
      this.toasterService.presentAlert(data?.message, data?.color);
    }
  }
}
