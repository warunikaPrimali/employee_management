/*  
* Describe the routes to Dashboard components 
*/

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActivateService } from '../core/activate.service';
import { ErrorPageComponent } from '../shared/error-page/error-page.component';
import { SummaryComponent } from './summary/summary.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: SummaryComponent, pathMatch: 'full', canActivate: [ActivateService] },
      { path: '**', component: ErrorPageComponent, pathMatch: 'full' },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
