import { AfterViewInit, Component, DoCheck, OnChanges, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Constants } from 'src/app/app-constants';
import { StorageService } from 'src/app/core/storage.service';
import { ToasterService } from 'src/app/shared/toaster.service';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
})
export class SummaryComponent implements OnInit, DoCheck {
  public empData: any;
  public setValue: boolean;

  constructor(private storageService: StorageService, private router: Router, private toasterService: ToasterService) {
  }

  ngOnInit() {
    this.getEmpData();
  }

  // TODO: FIXME: Can be improved with Redux State Management
  ngDoCheck() {
    if (localStorage.getItem(Constants.setValue) === 'true') {
      this.getEmpData();
      localStorage.setItem(Constants.setValue, 'false');
    }
  }

  /**
  * Method to get existing Employee data
  * @example
  * edit(1) 
  */
  async getEmpData() {
    this.empData = await this.storageService.getObject(Constants.person);
    this.setValue = false;
  }

  /**
  * Method to Navigate to Employee edit page
  * @param {Number} index - A Number parms to user Index 
  * @example
  * edit(1) 
  */
  edit(index) {
    this.router.navigate([Constants.routeEditEmployee, { id: index }]);
  }

  /**
  * Method to get a confirmation from User for particular User delete
  * @param {Number} index - A Number parm to user Index
  * call to the toaster service presentAlertPopup with relevant message Data  
  * on confirmation call to the delete method with index
  * @example
  * confirm(1) 
  */
  confirm(index) {
    const data = {
      message: 'Are you sure you want to delete ' + this.empData[index].fname,
      title: 'Delete',
      subTitle: '',
      btnColor: 'text-theme',
      btnText: 'OK'
    }
    this.toasterService.presentAlertPopup(data.message, data.title, data.btnColor, data.btnText, data.subTitle).then((data) => {
      if (data?.role === Constants.role) {
        this.deleteMethod(index);
      }
    })

  }

  /**
  * Method to delete an Employee
  * @param {Number} index - A Number parms to user Index
  * delete the particular user from storage  
  * Will show a toast on Successfull deletion
  * @example
  * deleteMethod(1) 
  */
  deleteMethod(index) {
    this.empData.splice(index, 1);
    this.storageService.setObject(Constants.person, this.empData).then((res) => {
      if (res) {
        const data = {
          message: 'Your employee has deleted successfully',
          color: 'favorite'
        }
        this.toasterService.presentAlert(data.message, data.color);
      }
    });
  }

  /**
  * Method to Navigate to Employee display page
  * @param {Number} index - A Number parms to user Index 
  * @example
  * dispalyEmp(1) 
  */
  dispalyEmp(index) {
    this.router.navigate([Constants.routeViewEmployee, { id: index }]);
  }
}
