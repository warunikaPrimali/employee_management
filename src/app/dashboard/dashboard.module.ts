/*  
* Module to Manage summary of the employee Details
*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { SummaryComponent } from './summary/summary.component';
import { IonicModule } from '@ionic/angular';


@NgModule({
  declarations: [
    DashboardComponent,
    SummaryComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    IonicModule
  ]
})
export class DashboardModule { }
