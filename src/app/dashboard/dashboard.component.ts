import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constants } from '../app-constants';
import { StorageService } from '../core/storage.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  public empData: any;

  constructor() { 
  }

  ngOnInit() {
  }
}
