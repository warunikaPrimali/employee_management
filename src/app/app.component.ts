import { Component } from '@angular/core';
import { Constants } from './app-constants';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})

/*  
* Application Component   
*/  
export class AppComponent {
  public user;
  public appPages = [
    { title: 'Dashboard', url: '/dashboard', icon: 'home' },
    { title: 'Add Employee', url: '/employee/create', icon: 'people' },
    { title: 'Logout', url: '/auth/login', icon: 'exit' }
  ]

/**
* This is the constructur
* In here will retrive and set the current loggedIn user details
*/ 
  constructor() { 
    this.user = {
      name: Constants.userCredentials.name,
      time: JSON.parse(sessionStorage.getItem(Constants.loggedTime))
    }
  }


/**
* This method will be called upon logout
* clear the local storage
*/   

  onClick(url) {
    if (url === Constants.routeLogin) {
      sessionStorage.removeItem(Constants.logged)
    }
  }
}
