/*  
* Activate Route service using status of loggedIn or Not
*/

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Constants } from '../app-constants';

@Injectable({
  providedIn: 'root'
})
export class ActivateService implements CanActivate {

  constructor(private router: Router, private toastController: ToastController) { }

  canActivate(): boolean {
    const logged = sessionStorage.getItem(Constants.logged);
    if (logged) {
      return true;
    } else {
      this.presentAlert()
      this.router.navigate([Constants.routeLogin]);
      return false;
    }
  }

  async presentAlert() {
    const toast = await this.toastController.create({
      message: 'Please Login to use this feature',
      duration: 3000,
    });
    toast.present();
  }

}
