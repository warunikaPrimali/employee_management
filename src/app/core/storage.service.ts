import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Constants } from '../app-constants';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private storage: Storage) {
    this.initStorage();
  }

  async initStorage() {
    await this.storage.create();
  }

  // set a key/value
  async set(key: string, value: any) {
    const result = await this.storage.set(key, value);
    console.log('result', result)
  }

  // to get a key/value pair
  async get(key: string) {
    const value = await this.storage.get(key);
    return value;
  }

  // set a key/value object
  async setObject(key: string, object: Object) {
    return await this.storage.set(key, JSON.stringify(object));
  }

  // get a key/value object
  async getObject(key: string) {
    const result = await this.storage.get(key);
    if (result != null) {
      return JSON.parse(result);
    }
  }

  // remove a single key value:
  remove(key: string) {
    this.storage.remove(key);
  }

  //  delete all data from your application:
  clear() {
    this.storage.clear();
  }
}
