import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() { }

  /*  
  * Custom String Validator method
  */
  nameValidator(control: FormControl): { [key: string]: any } {
    const nameRegexp = /^[A-Za-z0-9]{1,20}$/;
    if (control.value && !nameRegexp.test(control.value)) {
      return { invalidName: true };
    }
  }

  /*  
  * Custom White Space Validator method
  */
  whiteSpaceChecker(control: FormControl): { [key: string]: any } {
    const whiteSpaceRegexp = /^[^\s]+(\s+[^\s]+)*$/;
    if (control.value && !whiteSpaceRegexp.test(control.value)) {
      return { invalidText: true };
    }
  }

  /*  
  * Custom Email Validator method
  */
  emailValidator(control: FormControl): { [key: string]: any } {
    const emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (control.value && !emailRegexp.test(control.value)) {
      return { invalidEmail: true };
    }
  }

  /*  
  * Custom Numeric Validator method
  */
  numericValidator(control: FormControl): { [key: string]: any } {
    console.log('value', control.value);
    const numericRegexp = /^\d*$/;
    console.log('space', !numericRegexp.test(control.value))
    if (control.value && !numericRegexp.test(control.value)) {
      return { invalid: true };
    }
  }

  /*  
  * Custom mobile No Validator method
  */
  mobileValidator(control: FormControl): { [key: string]: any } {
    const mobileRegexp = /^[7]\d{8}$/;
    const mobileRegexp2 = /^[0][7]\d{8}$/;

    console.log(mobileRegexp.test(control.value), mobileRegexp2.test(control.value), !(mobileRegexp.test(control.value) || mobileRegexp2.test(control.value)));

    if (control.value && !(mobileRegexp.test(control.value) || mobileRegexp2.test(control.value))) {
      return { invalidMobile: true };
    }
  }
}
