/*  
* Describe the routes to employee Management components 
*/

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActivateService } from '../core/activate.service';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { EmployeeManageComponent } from './employee-manage/employee-manage.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'create', component: EmployeeManageComponent, canActivate: [ActivateService] },
      { path: 'edit', component: EmployeeManageComponent, canActivate: [ActivateService] },
      { path: 'show', component: EmployeeDetailsComponent, canActivate: [ActivateService] }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
