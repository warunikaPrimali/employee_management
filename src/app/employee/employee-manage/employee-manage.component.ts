import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Constants } from 'src/app/app-constants';
import { StorageService } from 'src/app/core/storage.service';
import { ValidationService } from 'src/app/core/validation.service';
import { ToasterService } from 'src/app/shared/toaster.service';

@Component({
  selector: 'app-employee-manage',
  templateUrl: './employee-manage.component.html',
  styleUrls: ['./employee-manage.component.scss'],
})
export class EmployeeManageComponent implements OnInit, AfterViewInit {
  empForm: FormGroup
  public empdata;
  index: any;

  constructor(private fb: FormBuilder, private storageService: StorageService, private router: Router, private activatedRoute: ActivatedRoute,
    private toasterService: ToasterService, private validationService: ValidationService) {
  }

  /**
  * Method to access UserId from route params
  * Call getData() function and access employee details match to provided userId
  * set those data to empdata variable and set it as empForm Value
  */
  async getPrevData() {
    this.index = this.activatedRoute.snapshot.params['id'];
    this.empdata = (await this.getData()) ? (await this.getData())[this.index] : null;
    console.log('empdata', this.empdata)
    if (this.empdata) {
      this.empForm.setValue(this.empdata)
    }
  }


  ngOnInit() {
    this.empForm = this.fb.group({
      fname: ['', [Validators.required]],
      lname: ['', [Validators.required]],
      gender: ['', Validators.required],
      dob: ['', Validators.required],
      email: ['', [Validators.required, this.validationService.emailValidator]],
      phone: ['', [Validators.required, this.validationService.mobileValidator]],
      address: ['', Validators.required],
      status: ['true', Validators.required]
    });
  }

  ngAfterViewInit() {
    this.getPrevData();
  }


  /**
  * Method to save or upadate user data upon form submit
  * @param {Object} data - A object parm contain employee data
  * @param {Number} index - A number parm (Index of the particular user)
  * Then save those data in the ionic storage
  */
  async manage(data?, index?) {
    let savedData = await this.getData();
    if (!data) {
      const oldData = savedData ? savedData : [];
      oldData.push(this.empForm.value);
      this.storageService.setObject(Constants.person, oldData).then((res) => {
        if (res) {
          const data = {
            message: 'Your employee ' + this.empForm.get('fname').value + ' has added successfully',
            color: 'favorite'
          }
          this.toasterService.presentAlert(data.message, data.color);
          this.router.navigate([Constants.routeDashboard])
        }
      });
    } else {
      savedData[index] = this.empForm.value;
      this.storageService.setObject(Constants.person, savedData).then((res) => {
        if (res) {
          const data = {
            message: 'Employee detaild of ' + this.empForm.get('fname').value + ' has updated successfully',
            color: 'favorite'
          }
          localStorage.setItem(Constants.setValue, 'true')
          this.toasterService.presentAlert(data.message, data.color);
          this.router.navigate([Constants.routeDashboard]);
        }
      });
    }
  }

  /**
  * Method to access storage service getObject method
  * @return {Object} - return an object contains employee data saved in the ionic storage
  */
  getData() {
    return this.storageService.getObject(Constants.person);
  }
}
