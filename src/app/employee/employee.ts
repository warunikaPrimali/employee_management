/*  
* Data modal of employee 
*/

export class Employee {
    fname: String;
    lname: String;
    gender: String;
    dob: any;
    email: String;
    phone: String;
    address: String;
    status: Boolean;

    constructor(obj) {
        this.fname = (obj || {}).fname;
        this.lname = (obj || {}).lname;
        this.gender = (obj || {}).gender;
        this.dob = new Date((obj || {}).dob).toLocaleDateString();
        this.email = (obj || {}).email;
        this.phone = (obj || {}).phone;
        this.address = (obj || {}).address;
        this.status = (obj || {}).status;
    }

}