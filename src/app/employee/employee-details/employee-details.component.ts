import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Constants } from 'src/app/app-constants';
import { StorageService } from 'src/app/core/storage.service';
import { Employee } from '../employee';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.scss'],
})
export class EmployeeDetailsComponent implements OnInit {
  public index;
  public empData: any;
  constructor(private activatedRoute: ActivatedRoute, private storageService: StorageService) { }

  ngOnInit() {
    this.getPrevData();
  }

/**
* Method to access UserId from route params
* Call getData() function and access employee details match to provided userId
* set those data to empdata variable
*/
  async getPrevData() {
    this.index = this.activatedRoute.snapshot.params['id'];
    const data = (await this.getData()) ? (await this.getData())[this.index] : null;
    this.empData = new Employee(data)
  }

/**
* Method to access storage service getObject method
* @return {Object} - return an object contains employee data saved in the ionic storage
*/
  getData() {
    return this.storageService.getObject(Constants.person);
  }
}
