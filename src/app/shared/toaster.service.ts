import { Injectable } from '@angular/core';
import { ToastController, AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToasterService {

  constructor(private toastController: ToastController, private alertController: AlertController) {
  }


  /**
  * Method to show a toast with provided arguments
  * @param {string} message - A string param for message  
  * @param {string} clr - A string param for ionic toast color
  * @example
  * presentAlert("Invalid username or password","warning") 
  */
  async presentAlert(message, clr?) {
    const toast = await this.toastController.create({
      message: message,
      duration: 3000,
      color: clr
    });
    toast.present();
  }

  /**
  * Method to load a toast with provided arguments
  * @param {string} message - A string param for message
  * @param {string} title - A string param  for popup title
  * @param {string} btnColor - A string/string[] param  for ionic button class
  * @param {string} btnText - A string param  for button text
  * @param {string} subtitle - A optional string parm for popup subtitle
  * @return {Object} return a object containg, the way alert is closed 
  * @example
  * presentAlertPopup("Successfull","Success","primary","OK")
  */
  async presentAlertPopup(message, title, btnColor, btnText, subtitle?) {
    const alert = await this.alertController.create({
      header: title,
      subHeader: subtitle,
      message: message,
      buttons: [
        {
          text: btnText,
          role: 'submit',
          cssClass: btnColor
        }
      ]
    });
    await alert.present();
    return await alert.onDidDismiss();
  }

}
