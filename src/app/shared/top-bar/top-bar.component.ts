import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constants } from 'src/app/app-constants';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
})
export class TopBarComponent implements OnInit {
  public page;

  constructor(private router: Router) {
   }

  ngOnInit() {
    const data = this.router.url.split(';')[0];
    switch (data) {
      case Constants.routeDashboard:
        this.page = 'Dashboard';
        break
      case Constants.routeEditEmployee:
        this.page = 'Edit Employee';
        break
      case Constants.routeViewEmployee:
        this.page = 'Employee Detail';
        break
      case Constants.routeCreateEmployee:
        this.page = 'Employee Add';
        break
      default:
        this.page = 'HR Manager System';
        break
    }
  }
}
