/*  
* This module to Maintained shared components and shared services  
*/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';
import { ErrorPageComponent } from './error-page/error-page.component';
import { ToasterService } from './toaster.service';
import { TopBarComponent } from './top-bar/top-bar.component';



@NgModule({
  declarations: [
    TopBarComponent,
    ErrorPageComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    TopBarComponent,
    ErrorPageComponent
  ],
  providers: [
    ToasterService
  ]
})
export class SharedModule { }
