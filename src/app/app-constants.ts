/*
 * Application Constant File
 * Describe all Constants used in the Application
 */


import {environment} from '../environments/environment'
export class Constants {
    public static readonly appVersions: any = environment.appVersions;
    public static readonly routeDashboard: string = '/dashboard';
    public static readonly routeLogin: string = '/auth/login';
    public static readonly routeEditEmployee: string = '/employee/edit';
    public static readonly routeViewEmployee: string = '/employee/show';
    public static readonly routeCreateEmployee: string = '/employee/create';

    public static readonly userCredentials: any =  environment.user;
    public static readonly person: string =  'person';
    public static readonly logged: string = 'logged';
    public static readonly loggedTime: string = 'logged_time';
    public static readonly role: string = 'submit';
    public static readonly setValue: string = 'updated';
}